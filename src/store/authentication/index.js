import authGetters from './getters.js';
import authActions from './actions.js';
import authMutations from './mutations.js';

export default {
  namespaced: true,
  state() {
    return {
      isLoggedIn: false
    };
  },
  mutations: authMutations,
  actions: authActions,
  getters: authGetters
};
