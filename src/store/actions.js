export default {
  addToCartAction(context, payload) {
    context.commit('addProductToCart', payload.value);
  },
  removeAction(context, payload) {
    context.commit('removeProductFromCart', payload);
  },
  addNewItem(context, payload) {
    context.commit('addNewItem', payload);
  },
  removeItem(context, payload) {
    context.commit('removeItem', payload);
  }
};
