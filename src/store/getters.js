export default {
  fetchProducts(state) {
    return state.products;
  },
  fetchCart(state) {
    return state.cart;
  },
  fetchCartTotal(state) {
    return state.cart.total;
  }
};
