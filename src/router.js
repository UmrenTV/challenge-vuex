import { createRouter, createWebHistory } from 'vue-router';
import store from './store/index.js';

import ProductsList from './pages/ProductsList.vue';
import UserCart from './pages/UserCart.vue';
import ShopAdmin from './pages/ShopAdmin.vue';
import Login from './pages/Login.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/products' },
    { path: '/products', component: ProductsList },
    { path: '/cart', component: UserCart },
    { path: '/login', component: Login },
    {
      path: '/add-new',
      name: 'admin',
      component: ShopAdmin,
      beforeEnter(_, _1, next) {
        if (store.getters['auth/getAuth']) {
          next();
        } else {
          router.push('/login');
        }
      }
    }
  ]
});

export default router;
